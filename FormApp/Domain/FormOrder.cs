﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class FormOrder
    {
        enum Statuses : byte
        {
            Student,
            Working,
            Retired,
            Other
        };

        public Guid Id { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set;}
        public byte Age { get; set; }
        public DateTime DateDesktop { get; set; }
        public string Status { get; set; } = Statuses.Other.ToString();
        public bool Github { get; set; }
        public bool Gitlab { get; set; }
        public bool Dockerhub { get; set; }
        public string? Image { get; set;}
    }
}
