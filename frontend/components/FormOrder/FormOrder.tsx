import { useForm, Controller } from 'react-hook-form';
import cn from 'classnames';
import {
  Button,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  TextField,
  RadioGroup,
  FormControlLabel,
  FormLabel,
  Radio,
  FormGroup,
  Checkbox,
  Input,
} from '@mui/material';
import SendIcon from '@mui/icons-material/Send';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DesktopDatePicker } from '@mui/x-date-pickers/DesktopDatePicker';
import { FormOrderArgs, Status } from '@app/interfaces';
import { FormOrderProps } from './FormOrder.props';

import styles from './FormOrder.module.css';

const FormOrder = ({
  handleSubmitFormOrderData,
  className,
  ...props
}: FormOrderProps) => {
  const { control, handleSubmit } = useForm<FormOrderArgs>({
    defaultValues: {
      firstName: 'Vasya',
      lastName: 'Pupkin',
      age: 10,
      dateDesktop: new Date(),
      status: Status.OTHER,
      github: true,
      gitlab: false,
      dockerhub: false,
      image: '',
    },
  });

  return (
    <form
      className={cn(styles.formOrder, className)}
      onSubmit={handleSubmit(handleSubmitFormOrderData)}
      {...props}
    >
      <Controller
        name="firstName"
        control={control}
        render={({ field }) => (
          <TextField
            fullWidth
            label="First Name"
            variant="outlined"
            margin="normal"
            {...field}
          />
        )}
      />
      <Controller
        name="lastName"
        control={control}
        render={({ field }) => (
          <TextField
            fullWidth
            label="Last Name"
            variant="outlined"
            margin="normal"
            {...field}
          />
        )}
      />
      <FormControl fullWidth margin="normal">
        <InputLabel id="selectAge">Age</InputLabel>
        <Controller
          name="age"
          control={control}
          render={({ field }) => (
            <Select labelId="selectAge" label="Age" {...field}>
              <MenuItem value={10}>Ten</MenuItem>
              <MenuItem value={20}>Twenty</MenuItem>
              <MenuItem value={30}>Thirty</MenuItem>
            </Select>
          )}
        />
      </FormControl>
      <FormControl fullWidth margin="normal">
        <LocalizationProvider dateAdapter={AdapterDateFns}>
          <Controller
            name="dateDesktop"
            control={control}
            render={({ field }) => (
              <DesktopDatePicker
                label="Date desktop"
                inputFormat="yyyy-MM-dd"
                renderInput={(params) => <TextField {...params} />}
                {...field}
              />
            )}
          />
        </LocalizationProvider>
      </FormControl>
      <FormControl fullWidth margin="normal">
        <FormLabel id="demo-row-radio-buttons-group-label">Status</FormLabel>
        <Controller
          name="status"
          control={control}
          render={({ field }) => (
            <RadioGroup
              row
              aria-labelledby="demo-row-radio-buttons-group-label"
              {...field}
            >
              <FormControlLabel
                value="Student"
                control={<Radio />}
                label="Student"
              />
              <FormControlLabel
                value="Working"
                control={<Radio />}
                label="Working"
              />
              <FormControlLabel
                value="Retired"
                control={<Radio />}
                label="Retired"
              />
              <FormControlLabel
                value="Other"
                control={<Radio />}
                label="Other"
              />
            </RadioGroup>
          )}
        />
      </FormControl>
      <FormGroup>
        <FormLabel>Tags</FormLabel>
        <Controller
          name="github"
          control={control}
          render={({ field }) => (
            <FormControlLabel
              control={<Checkbox defaultChecked />}
              label="github"
              {...field}
            />
          )}
        />
        <Controller
          name="gitlab"
          control={control}
          render={({ field }) => (
            <FormControlLabel
              control={<Checkbox />}
              label="gitlab"
              {...field}
            />
          )}
        />
        <Controller
          name="dockerhub"
          control={control}
          render={({ field }) => (
            <FormControlLabel
              control={<Checkbox />}
              label="dockerhub"
              {...field}
            />
          )}
        />
      </FormGroup>
      <FormControl fullWidth margin="normal">
        <Controller
          name="image"
          control={control}
          render={({ field }) => <Input type="file" {...field} />}
        />
      </FormControl>
      <Button
        type="submit"
        className={styles.submitButton}
        variant="contained"
        endIcon={<SendIcon />}
      >
        Send
      </Button>
    </form>
  );
};

export { FormOrder };
