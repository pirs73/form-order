import { DetailedHTMLProps, HTMLAttributes } from 'react';

export interface FormOrderProps
  extends DetailedHTMLProps<HTMLAttributes<HTMLFormElement>, HTMLFormElement> {
  handleSubmitFormOrderData: (data: any) => void;
}
