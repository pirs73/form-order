export enum Status {
  STUDENT = 'Student',
  WORKING = 'Working',
  RETIRED = 'Retired',
  OTHER = 'Other',
}

export interface FormOrderArgs {
  firstName: string;
  lastName: string;
  age: number;
  dateDesktop: Date;
  status: Status;
  github: boolean;
  gitlab: boolean;
  dockerhub: boolean;
  image: string;
}
