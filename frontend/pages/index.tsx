import type { NextPage } from 'next';
import { SubmitHandler } from 'react-hook-form';
import { Box, Container, Typography } from '@mui/material';
import { BaseLayout } from '@app/layouts';
import { FormOrder } from '@app/components';

const Home: NextPage = () => {
  const handleSubmitFormOrderData: SubmitHandler<any> = (data) => {
    console.log('form data:', data);
  };

  return (
    <BaseLayout
      title="Form Order"
      description="Form Order Generated by create next app"
    >
      <Container maxWidth="sm">
        <Box
          sx={{
            width: 500,
            my: 4,
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <Typography variant="h4" component="h1" gutterBottom>
            Form Order
          </Typography>
          <FormOrder handleSubmitFormOrderData={handleSubmitFormOrderData} />
        </Box>
      </Container>
    </BaseLayout>
  );
};

export default Home;
