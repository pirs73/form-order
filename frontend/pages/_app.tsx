import type { AppProps } from 'next/app';
import CssBaseline from '@mui/material/CssBaseline';
import '../assets/css/styles.css';

export default function App({ Component, pageProps }: AppProps) {
  return (
    <>
      <CssBaseline />
      <Component {...pageProps} />
    </>
  );
}
